<?php

namespace flowsa\flowweather\migrations;

use Craft;
use craft\db\Migration;
use flowsa\flowweather\services\DarkSky;

/**
 * Install migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Place installation code here...
        $this->createTable('{{%flow_weather}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(255)->unique(),
            'date' => $this->date()->notNull(),
            'icon' => $this->string(500),
            'high' => $this->integer(),
            'low' => $this->integer(),
            'summary' =>  $this->string(500),
            'dateUpdated' => $this->dateTime()->notNull(),
            'dateCreated' => $this->dateTime()->notNull()
        ]);

        DarkSky::syncForecast();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTableIfExists('{{%flow_weather}}');
    }
}
