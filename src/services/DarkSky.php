<?php
/**
 * Flow Weather plugin for Craft CMS 3.x
 *
 * Weather Plugin by Flow Communications
 *
 * @link      http://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communcations
 */

namespace flowsa\flowweather\services;

use flowsa\flowweather\FlowWeather;

use Craft;
use craft\base\Component;

/**
 * DarkSky Service
 *
 * All of your plugin�s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Flow Communcations
 * @package   FlowWeather
 * @since     1.0.0
 */
class DarkSky extends Component
{
    // Public Methods
    // =========================================================================

    public static function syncForecast()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.darksky.net/forecast/'.FlowWeather::$plugin->getSettings()->apiKey.'/'.FlowWeather::$plugin->getSettings()->coords.'?exclude=minutely,hourly,flags,alerts&units=ca');
        $body = json_decode((string) $res->getBody());

        foreach ($body->daily->data as $forecast) {
            self::insertForecast(self::transformForecast($forecast));
        }

        self::insertForecast(self::transformCurrent($body->currently));
    }

    private static function transformForecast($forecast)
    {
        return [
            'date' => date('Y-m-d', $forecast->time),
            'high' => (int) $forecast->temperatureHigh,
            'low' => (int) $forecast->temperatureLow,
            'icon' => $forecast->icon,
            'summary' => $forecast->summary
        ];
    }

    private static function transformCurrent($forecast)
    {
        return [
            'date' => date('Y-m-d', $forecast->time),
            'high' => (int) $forecast->temperature,
            'low' => (int) $forecast->temperature,
            'icon' => $forecast->icon,
            'summary' => $forecast->summary
        ];
    }

    private static function insertForecast($item)
    {
        \Craft::$app->db->createCommand()
        ->insert('{{%flow_weather}}', $item)
        ->execute();
    }
}
