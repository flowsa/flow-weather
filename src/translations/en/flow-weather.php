<?php
/**
 * Flow Weather plugin for Craft CMS 3.x
 *
 * Weather Plugin by Flow Communications
 *
 * @link      http://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communcations
 */

/**
 * Flow Weather en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('flow-weather', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow Communcations
 * @package   FlowWeather
 * @since     1.0.0
 */
return [
    'Flow Weather plugin loaded' => 'Flow Weather plugin loaded',
];
