<?php
/**
 * Flow Weather plugin for Craft CMS 3.x
 *
 * Weather Plugin by Flow Communications
 *
 * @link      http://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communcations
 */

/**
 * Flow Weather config.php
 *
 * This file exists only as a template for the Flow Weather settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'flow-weather.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
