<?php
/**
 * Flow Weather plugin for Craft CMS 3.x
 *
 * Weather Plugin developed by Flow Communications
 *
 * @link      http://www.flowsa.com
 * @copyright Copyright (c) 2019 Flow Communcations
 */

namespace flowsa\flowweather\variables;

use flowsa\flowweather\FlowWeather;

use Craft;

/**
 * Flow Weather Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.flowWeather }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Flow Communcations
 * @package   FlowWeather
 * @since     1.0.0
 */
class FlowWeatherVariable
{
    public function forecast($date = 'today')
    {
        $date = strtotime($date);

        $weather = (new \craft\db\Query())
        ->select(['high', 'low', 'icon'])
        ->from('{{%flow_weather}}')
        ->where(['date' => date('Y-m-d', $date)])
        ->orderBy(['dateUpdated' => SORT_DESC])
         ->one();

        return $weather;
    }
}
