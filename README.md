# Flow Weather plugin for Craft CMS 3.x

Weather Plugin developed by Flow Communications

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

# Flow Weather

## Installation

Add the repository to your repository listing in `composer.json`

```
// composer.json
"repositories":
{
    /// Other repos here
    {
        "type": "vcs",
        "url": "https://bitbucket.org/flowsa/flow-weather.git"
    }
}
```

Then run `composer require flowsa/flow-weather`

Before installation add 2 environment variables to your `.env` file

```
DARKSKY_KEY=deb176190eb7ce2087d253cf4f21402c
WEATHER_COORDS=-20.88,55.4481
```

`WEATHER_COORDS` is a set of the co-ordinates of where the weather must be drawn from
`DARKSKY_KEY` is your API key from [DarkSky](https://darksky.net/dev)

In the Control Panel, go to Settings → Plugins and click the “Install” button for Flow Weather.

Finally, add a cron job to sync the weather data
This cron should do a `GET` request to `actions/flow-weather/weather/forecast`

## Using Flow Weather

The Flow Weather plugin exposes an variable for consumption within your templates namely: `craft.flowWeather.forecast`

This forecast can be altered to differing days by passing the arguement of a string date (anything that can be consumed by `strtotime`), eg to get tomorrow's forecast you can use `craft.flowWeather.forecast('+1 day')`

This will return an array containing the following:

* `high`: temperature high for the day in question
* `low`: temperature low for the day in question
* `icon`: a string representing the conditions.
    * Options are: `clear-day`, `clear-night`, `partly-cloudy-day`, `partly-cloudy-night`, `cloudy`, `rain`, `sleet`, `snow`, `wind`, `fog`

These can therefore be accessed via `craft.flowWeather.forecast.high`

Brought to you by [Flow Communcations](http://www.flowsa.com)
